"""
Flask app for server

Author: Adam Case
Credits:
    -Kenny Nguyen (Peer)
    Reason: Help on how to get the full request path using
    request.environ['REQUEST_URI']
    -Flask Website
    URL: https://flask.palletsprojects.com
    Reason: Usage of Flask methods
    -regular expressions 101
    URL: https://regex101.com/
    Reason: Regular expresion testing
    -re
    URL: https://docs.python.org/3/library/re.html
    Reason: Understanding usage of regular expressions
"""


from flask import *
import os, re, werkzeug, re

app = Flask(__name__)

FORBIDDEN_PAT = r"(\/\/)|(\/\.\.)|(\/\~)"
ALLOWED_FILE_TYPES = r'((.html)|(.css))$'


def is_allowed_file_type(path):
    allowed_search = re.search(ALLOWED_FILE_TYPES, path)
    if allowed_search is None:
        return False
    return True

class PageError(werkzeug.exceptions.HTTPException):
    def __init__(self, code=520, url=""):
        self.code = code
        self.url = url

@app.route('/<path:path_in_templates>')
def file_req(path_in_templates):
    full_path = "./templates" + request.environ['REQUEST_URI']
    forbidden_search = re.search(FORBIDDEN_PAT, full_path)
    if forbidden_search is not None:
        raise PageError(403)
    elif os.path.exists(full_path) and is_allowed_file_type(full_path):
        resp = make_response(render_template(path_in_templates), 200)
        return resp
    else:
        raise PageError(404, request.environ['REQUEST_URI'])

@app.errorhandler(PageError)
def page_error(error):
    return render_template(f'{error.code}.html', url=error.url), error.code


if __name__ == "__main__":
    app.run(port=6567, debug=True, host='0.0.0.0')
