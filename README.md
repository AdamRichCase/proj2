# Purpose

* Get used to using flask to build web applications.
* Implement the same logic as is done in the previous project. Reference:
 https://bitbucket.org/AdamRichCase/proj1/src/master/

# Author

Name: Adam Case

Email: acase@uoregon.edu

# Usage
## Running the Server
* Use ```docker``` to build image from ```Dockerfile```
* After making the image, run using ```docker run -d -p <host-port>:6567 <image-name>```
## Tests
* Use the ```tests.sh``` script under "tests" folder to test the expected
outcomes
