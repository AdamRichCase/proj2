#! /bin/bash
#
# Test the trivial web server.
# Usage:  tests.sh http://the.IP.address:port/path
# Example: tests.sh http://127.0.0.1:5000/sample.html
#
URLbase=$1

# Test cases for the body
#

function expect_body() {
    # Args
    path=$1
    expect=$2
    curl --silent ${URLbase}/${path} >/tmp/,$$
    if grep -q ${expect} /tmp/,$$ ; then
	echo "Pass --  found ${expect} in ${path}"
    else
        echo "*** FAIL *** expecting ${expect} in  ${URLbase}/${path}"
    fi
}

function expect_status() {
    # Args
    path=$1
    expect=$2
    curl --silent -i ${URLbase}/${path} >/tmp/,$$
    if grep -q ${expect} /tmp/,$$ ; then
	echo "Pass --  found ${expect} in ${URLbase}/${path} "
    else
        echo "*** FAIL *** expecting status ${expect} in ${URLbase}/${path} "
    fi
}


echo "Standard Tests"
expect_body trivia.html  "Seriously"
expect_status trivia.html "200"
echo "Page not found Tests"
expect_body nosuch.html "File not found!"
expect_status nosuch.html "404"
expect_status there/theybe.html "404"
#Forbidden Entries
echo "Forbidden Tests"
expect_body there//theybe.html "ERROR 403"
expect_status there//theybe.html "403"
expect_status /mightbe.html "403"
expect_status ~forbidden/directory "403"
expect_status another/~forbidden.html "403"
expect_status another/..forbidden.html "403"
#My other test cases in subfolders
echo "Subfolder Tests"
expect_body my_tests/my_test.html "My Test"
expect_status my_tests/inaccessable_file.txt "404" #incorrect file type
